<?php

$settings = array(
		'source' => 'C:\xampp\htdocs\www.nu-me.eu\app\locale\en_US',
		'translations' => 'C:\xampp\htdocs\www.nu-me.eu\app\locale\hr_HR',
		'result' => 'c:\parser\result',
		'update' => 'c:\parser\update.csv', 
		'diff' => 'c:\parser\diff.csv'
);

echo "Starting parser\n\n";

if (file_exists($settings['diff'])) unlink($settings['diff']);

$files = array_diff(scandir($settings['source']), array('..', '.'));
$resultArray = array();
$diffArray = array();		

$updateTranslations = (file_exists($settings['update']))?
								getFileAsArray($settings['update']):
								array();
								
$counter = 0;

foreach($files as $file) {
	$sourceFile = $settings['source'] . '\\' . $file;
	
	if (!is_dir($sourceFile)) {
		echo "Processing: " . $sourceFile . ".\n";
		$sourceTranslations = getFileAsArray($sourceFile);
		foreach($sourceTranslations as $key => $value) {
			$translation = getTranslation($key, $file);
			$resultArray[$key] = $translation;
			//echo "\"" . $key . "\",\"" . $translation . "\"\n";
		}
		$resultFile = $settings['result'] . '\\' . $file;
		if (file_exists($resultFile)) unlink($resultFile);
		outputCSV($resultArray, $resultFile);
		echo "\n" . $resultFile . " done.\n\n";
	}
}

function getTranslation($key, $sourceFile) {
	global $settings;
	global $updateTranslations;
	$translationsFile = $settings['translations'] . '\\' . $sourceFile;
	if (file_exists($translationsFile)) {
		$translations = getFileAsArray($translationsFile);
		if (array_key_exists($key, $translations) && ($key != $translations[$key])) {
			echo '.';
			return $translations[$key];
		} else if (array_key_exists($key, $updateTranslations)) { 
			echo '#';
			return $updateTranslations[$key];
		} else {
			echo '*';
			return $key;
		}
	}
}


function getFileAsArray($file) {
	$translations = array();
	if (file_exists($file)) {
		ini_set('auto_detect_line_endings',TRUE);
		$handle = fopen($file,'r');
		while ( ($data = fgetcsv($handle) ) !== FALSE ) {
			if (array_key_exists(0, $data) == true && array_key_exists(1, $data) == true) {
				$translations[$data[0]] = $data[1];
			} else {
				echo "Error in file " . $file . " [" . implode($data) . "]\n";
				die();
			}
		}
		ini_set('auto_detect_line_endings',FALSE);
	}
	return $translations;
}

function outputCSV($data, $file) {
	global $settings;
	$outputBuffer = fopen($file, 'w') or die("Unable to open " . $file . " file!");
	$diffBuffer = fopen($settings['diff'], 'a') or die("Unable to open diff file!");
	foreach($data as $key => $value) {
		$key = str_replace('"', '\"', $key);
		$value = str_replace('"', '\"', $value);
		$string = "\"" . $key . "\",\"" . $value . "\"\n";
		fwrite( $outputBuffer, $string);
		if ($key == $value)
			fwrite( $diffBuffer, $string);
	}
	fclose($diffBuffer);
	fclose($outputBuffer);
}

echo "\nFinished.\n";